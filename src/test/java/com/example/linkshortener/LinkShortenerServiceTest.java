package com.example.linkshortener;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

class LinkShortenerServiceTest {

    Logger logger = LoggerFactory.getLogger(LinkShortenerServiceTest.class);

    @Test
    void appendHttps() {
        String s = "google.com";
        assertEquals("https://google.com", LinkShortenerService.appendHttps(s));
    }

    @Test
    void testIdCollisions() {
        HashSet<String> set = new HashSet<>();
        final int limit = 1000 * 1000;
        int collisionsCount = 0;

        for (int i = 0; i < limit; i++) {
            String id = LinkShortenerService.randomString(4);
            while (set.contains(id)) {
                id = LinkShortenerService.randomString(4);
                collisionsCount++;
            }
            set.add(id);
        }

        logger.debug("Collisions counter: " + collisionsCount);

        assertEquals(set.size(), limit);
    }
}