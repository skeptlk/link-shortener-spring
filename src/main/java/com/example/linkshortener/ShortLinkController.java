package com.example.linkshortener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletResponse;

@Controller
public class ShortLinkController {

    Logger logger = LoggerFactory.getLogger(LinkShortenerApplication.class);

    @Autowired
    private LinkShortenerService service;

    @RequestMapping("/{id}")
    public void resolveLink(@PathVariable String id,
                            HttpServletResponse httpServletResponse) throws Exception {
        String fullLink = service.resolveLink(id);
        logger.debug("Link resolved: " + id + " = " + fullLink);

        httpServletResponse.setStatus(302);
        httpServletResponse.setHeader("Location", fullLink);
    }

    @GetMapping("/")
    public String homePage() {
        return "create-link";
    }

    @GetMapping("/create")
    public String createLinkPage() {
        return "create-link";
    }

    @PostMapping("/create")
    public void createLink(@RequestParam(name = "link") String fullLink,
                             HttpServletResponse httpServletResponse) {
        String link = service.createShortLink(fullLink);
        logger.debug("Short link created: " + fullLink + " = " + link);

        if (link != null)
            httpServletResponse.setHeader("Location", "/createSuccess?link=" + link);
        else
            httpServletResponse.setHeader("Location", "/error");

        httpServletResponse.setStatus(302);
    }

    @GetMapping("/createSuccess")
    public String createSuccess(@RequestParam(name = "link") String link, Model model) {
        model.addAttribute("link", link);
        return "success";
    }

    @ExceptionHandler(Exception.class)
    @GetMapping("/error")
    public String errorPage(Exception e, Model model) {
        model.addAttribute("error", e.getMessage());
        return "error";
    }

}
