package com.example.linkshortener;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

@Configuration
public class JedisPoolFactory {
    @Value("${spring.redis.host}")
    private String host;

    @Value("${spring.redis.port}")
    private int port;

    /**
     * Initialize Redis connection pool
     */
    @Bean
    public JedisPool generateJedisPoolFactory() {
        JedisPoolConfig poolConfig = new JedisPoolConfig();
        // Whether to block when the connection is exhausted, false will report an exception, true will block until the timeout, and the default is true
        poolConfig.setBlockWhenExhausted(Boolean.TRUE);
        // If the Redis password is set, please call the following constructor
        //  JedisPool jedisPool = new JedisPool(poolConfig, host, port, timeout, password);
        return new JedisPool(poolConfig, host, port, 300);
    }
}