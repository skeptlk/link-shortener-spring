package com.example.linkshortener;

import com.example.linkshortener.models.Link;
import org.springframework.data.repository.CrudRepository;

public interface LinksRepository extends CrudRepository<Link, Integer> {
    Link findFirstByShortened(String shortened);
}
