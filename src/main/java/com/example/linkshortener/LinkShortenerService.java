package com.example.linkshortener;

import com.example.linkshortener.models.Link;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.exceptions.JedisConnectionException;
import java.security.SecureRandom;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Business logic goes here
 */

@Service
public class LinkShortenerService {

    @Autowired
    private JedisPool jedisPool;

    @Autowired
    private LinksRepository linksRepository;

    public String resolveLink(String id) throws JedisConnectionException, Exception {
        // check in redis
        Jedis jedis = null;
        String fullLink = null;

        jedis = jedisPool.getResource();
        fullLink = jedis.get(id);

        // search in database if not found in redis
        if (fullLink == null) {
            Link rec = linksRepository.findFirstByShortened(id);
            fullLink = rec.getFull();
            if (jedis != null)
                jedis.set(id, fullLink);
        }

        if (fullLink == null)  throw new Exception("Link not found!");

        if (jedis != null) jedis.close();

        return fullLink;
    }

    public String createShortLink(String fullLink) {
        try {
            Jedis jedis = jedisPool.getResource();

            fullLink = appendHttps(fullLink);
            String shortLink;

            System.out.println(fullLink);

            do {
                shortLink = randomString(4);
                System.out.println("Short " + shortLink);

                // check if already used
                if (linksRepository.findFirstByShortened(shortLink) != null)
                    continue;
                break;
            } while (true);

            Timestamp timestamp = new Timestamp(new Date().getTime());
            Link record = new Link(0, timestamp, fullLink, shortLink);
            linksRepository.save(record);

            jedis.set(shortLink, fullLink);
            jedis.close();

            return shortLink;

        } catch (Exception e) {
            System.err.println("Exception: " + e.getMessage());
            return null;
        }
    }

    public static String appendHttps(String link) {
        if (link.indexOf("http") != 0)
            return "https://" + link;
        else return link;
    }

    static final String AB = "0123456789abcdefghijklmnopqrstuvwxyz";
    static SecureRandom rnd = new SecureRandom();

    public static String randomString(int len){
        StringBuilder sb = new StringBuilder(len);
        for(int i = 0; i < len; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();
    }


}
