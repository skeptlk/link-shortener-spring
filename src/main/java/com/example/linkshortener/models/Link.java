package com.example.linkshortener.models;

import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Link {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;
    private java.sql.Timestamp created;
    private String full;
    private String shortened;

    public Link() { }

    public Link(Integer id, Timestamp created, String full, String shortened) {
        this.id = id;
        this.created = created;
        this.full = full;
        this.shortened = shortened;
    }

    public long getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public String getFull() {
        return full;
    }

    public void setFull(String full) {
        this.full = full;
    }

    public String getShortened() {
        return shortened;
    }

    public void setShortened(String shortened) {
        this.shortened = shortened;
    }
}

