# Simple link shortener

Requirements: 
- Java 11
- Passwordless redis server running on port 6379
- MySQL instance on port 3306

Run: 

```
mvn clean package
java -jar link-shortener-0.0.1-SNAPSHOT.jar
```

redis.conf (to autometically remove least frequently used keys)
```
maxmemory 1mb 
maxmemory-policy allkeys-lfu
```

In LinkShortenerServiceTest you can find an interesting benchmark.